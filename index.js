const express = require('express');
const routes = require('./routes');
const mongoose = require('mongoose');
const port = 3001;
const app = express();
const cors = require('cors')

app.use(cors())

app.use(express.json());

mongoose.connect('mongodb://localhost:27017/demo-be')
    .then(() => {
        require('./models/Item')
        routes(app);
        app.listen(port, () => {
            console.log('Connected on port: ', port)
        })
    })
    .catch((err) => console.log('err: ', err))



