const { Schema, model } = require('mongoose');
const Item = new Schema({
    name: {
        type: String,
        required: [true, 'thieu name']
    } 
});

module.exports = model('item', Item);