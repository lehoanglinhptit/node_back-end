module.exports = (app) => {
    const services = require('../services/ItemHandler')

    // //API get list
    // app.get('/items', services.getListItem)

    // //API get by ID
    // app.get('/items/:id', services.getItemByID)

    // //API create
    // app.post('/items', services.createItem)
    // //API put 
    // app.put('/items/:id',services.updateItem)
    // //API delete
    // app.delete('/items/:id',services.deleteItem)

    app.route('/items')
        .get(services.getListItem)
        .post(services.createItem)

    app.route('/items/:id')
        .get(services.getItemByID)
        .put(services.updateItem)
        .delete(services.deleteItem)
}