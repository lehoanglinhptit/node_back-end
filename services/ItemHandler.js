const { model } = require('mongoose'),
    Item = model('item')

exports.getListItem = (req, res) => {
    const { textSearch, limit, pageIdx } = req.query
    if (parseInt(limit) > 0 && parseInt(pageIdx) > 0) {
        Item.find({
            name: { $regex: textSearch, $options: 'i' }
        }, { __v: 0 })
            .skip(limit * (pageIdx - 1))
            .limit(limit)
            .then(async (data) => {
                const totalRecord = await Item.countDocuments({ name: { $regex: textSearch, $options: 'i' } })
                res.json({
                    data,
                    totalPage: Math.ceil(totalRecord / limit)
                })
            })
            .catch(err => res.send(err))
    } else {
        res.status(400).send({ message: "Sai kieu cua pageIdx hoac limit" })
    }

}

exports.getItemByID = (req, res) => {
    const { id } = req.params //path params
    Item.findById(id, { __v: 0 })
        .then(data => res.json(data))
        .catch(err => res.send(err))
}

exports.createItem = (req, res) => {
    const newData = new Item(req.body)
    newData.save()
        .then(data => res.json(data))
        .catch(err => res.send(err))
}

exports.updateItem = (req, res) => {
    const { id } = req.params
    Item.findByIdAndUpdate(id, req.body, { new: true })
        .then(data => res.json(data))
        .catch(err => res.send(err))

}
exports.deleteItem = (req, res) => {
    const { id } = req.params
    Item.findByIdAndDelete(id)
        .then(data => res.json(data))
        .catch(err => res.send(err))
}